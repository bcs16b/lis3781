> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Bernard Schramm

### Assignment 2 Requirements:

*3 Parts:*

1. Create bcs16b database and customer and company tables
2. Populate tables with at least 5 records each
3. Create 2 users (user3 and user4)

#### README.md file should include the following items:

* Screenshot of A2 SQL Code
* Screenshot of populated tables

#### Assignment Screenshots:

#### Screenshot of A2 SQL Code:

![A2 SQL Code Screenshot 1](img/a2_sql_code_a.png)
![A2 SQL Code Screenshot 2](img/a2_sql_code_b.png)
![A2 SQL Code Screenshot 3](img/a2_sql_code_c.png)
![A2 SQL Code Screenshot 4](img/a2_sql_code_d.png)
#### Screenshot of Populated Tables:
##### Company Table:
![Company Table Screenshot](img/a2_company_table.png)
##### Customer Table:
![Customer Table Screenshot](img/a2_customer_table.png)
