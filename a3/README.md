> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Bernard Schramm

### Assignment 3 Requirements:

*3 Parts:*

1. Log into Oracle Server using RemoteLabs
2. Create and populate Oracle tables
3. Generate reports

#### README.md file should include the following items:

* Screenshot of A3 SQL Code
* Screenshot of populated tables

#### Assignment Screenshots:

#### Screenshot of A3 SQL Code:

![A2 SQL Code Screenshot 1](img/a3_sql_code_a.png)
![A2 SQL Code Screenshot 2](img/a3_sql_code_b.png)
![A2 SQL Code Screenshot 3](img/a3_sql_code_c.png)
#### Screenshot of Populated Tables:
##### Company Table:
![Company Table Screenshot](img/a3_com_table.png)
##### Customer Table:
![Customer Table Screenshot](img/a3_cus_table.png)
##### Order Table:
![Order Table Screenshot](img/a3_ord_table.png)
