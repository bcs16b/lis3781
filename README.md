> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 3781 - Advanced Database Management

## Bernard Schramm

### LIS 3781 Requirements: 

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - A2 Bitbucket requirements will be put w/in A1

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Use RemoteLabs to login to Oracle Server
    - Create and populate tables
    - Generate reports

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Log into MS SQL Server
    - Create and populate tables
    - Generate reports

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Log into MS SQL Server
    - Create and populate tables / modify existing tables
    - Generate reports

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Screenshot of ERD
    - Screenshot of P1 SQL Code
    - Screenshot of populated tables

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - TBD
