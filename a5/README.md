> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Bernard Schramm

### Assignment 5 Requirements:

*3 Parts:*

1. Log into MS SQL Server
2. Create and populate tables / modify existing tables
3. Generate reports

#### README.md file should include the following items:

* Screenshot of A5 ERD

#### Assignment Screenshots:

![A5 ERD](img/A5_ERD.png)