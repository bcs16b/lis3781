> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Bernard Schramm

### Project 1 Requirements:

*3 Parts:*

1. Create tables and layers in ERD
2. Populate tables
3. Generate reports

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of P1 SQL Code
* Screenshot of populated tables

#### Project Screenshots:

#### Screenshot of P1 ERD:

![P1 ERD](img/P1_ERD.png)

#### Screenshot of P1 SQL Code:

![P1 SQL Code Screenshot 1](img/p1_sql_code_a.png)
![P1 SQL Code Screenshot 2](img/p1_sql_code_b.png)
![P1 SQL Code Screenshot 3](img/p1_sql_code_c.png)
![P1 SQL Code Screenshot 4](img/p1_sql_code_d.png)
![P1 SQL Code Screenshot 5](img/p1_sql_code_e.png)
![P1 SQL Code Screenshot 6](img/p1_sql_code_f.png)
![P1 SQL Code Screenshot 7](img/p1_sql_code_g.png)
![P1 SQL Code Screenshot 8](img/p1_sql_code_h.png)
![P1 SQL Code Screenshot 9](img/p1_sql_code_i.png)
![P1 SQL Code Screenshot 10](img/p1_sql_code_j.png)
![P1 SQL Code Screenshot 11](img/p1_sql_code_k.png)
![P1 SQL Code Screenshot 12](img/p1_sql_code_l.png)
![P1 SQL Code Screenshot 13](img/p1_sql_code_m.png)
![P1 SQL Code Screenshot 14](img/p1_sql_code_n.png)

#### Screenshot of Populated Tables:

##### Assignment Table:
![Assignment Table Screenshot](img/p1_asn_table.png)
##### Attorney Table:
![Attorney Table Screenshot](img/p1_atn_table.png)
##### Bar Table:
![Bar Table Screenshot](img/p1_bar_table.png)
##### Case Table:
![Case Table Screenshot](img/p1_cse_table.png)
##### Client Table:
![Client Table Screenshot](img/p1_cli_table.png)
##### Court Table:
![Court Table Screenshot](img/p1_crt_table.png)
##### Judge Table:
![Judge Table Screenshot](img/p1_jud_table.png)
##### Judge History Table:
![Judge History Table Screenshot](img/p1_jhs_table.png)
##### Phone Table:
![Phone Table Screenshot](img/p1_phn_table.png)
##### Specialty Table:
![Specialty Table Screenshot](img/p1_spc_table.png)
##### Person Table:
![Person Table Screenshot 1](img/p1_per_table1.png)
![Person Table Screenshot 2](img/p1_per_table2.png)